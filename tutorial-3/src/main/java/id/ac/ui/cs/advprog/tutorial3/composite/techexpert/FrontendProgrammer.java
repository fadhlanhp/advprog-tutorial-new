package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, Double salary) {
        this.name = name;
        if (salary >= 30000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary terlalu rendah!");
        }
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
