package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
        this.description = "cucumber";
    }

    @Override
    public String getDescription() {
        String foodDescription = food.getDescription();
        return foodDescription + ", adding " + description;
    }

    @Override
    public double cost() {
        double foodCost = food.cost();
        return foodCost + 0.4;
    }
}
