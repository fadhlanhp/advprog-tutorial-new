package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, Double salary) {
        this.name = name;
        if (salary >= 50000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary terlalu rendah!");
        }
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
