package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
        this.description = "cheese";
    }

    @Override
    public String getDescription() {
        System.out.println("test");
        String foodDescription = food.getDescription();
        return foodDescription + ", adding " + description;
    }

    @Override
    public double cost() {
        double foodCost = food.cost();
        return foodCost + 2.0;
    }
}