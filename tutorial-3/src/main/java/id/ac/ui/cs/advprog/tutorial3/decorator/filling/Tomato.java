package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        this.description = "tomato";
    }

    @Override
    public String getDescription() {
        String foodDescription = food.getDescription();
        return foodDescription + ", adding " + description;
    }

    @Override
    public double cost() {
        double foodCost = food.cost();
        return foodCost + 0.5;
    }
}
