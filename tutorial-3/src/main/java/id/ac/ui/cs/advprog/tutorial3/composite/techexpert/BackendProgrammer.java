package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public BackendProgrammer(String name, Double salary) {
        this.name = name;
        if (salary >= 20000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary terlalu rendah!");
        }
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
