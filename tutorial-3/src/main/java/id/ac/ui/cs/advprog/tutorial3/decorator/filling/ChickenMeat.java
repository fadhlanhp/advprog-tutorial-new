package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food;
        this.description = "chicken meat";
    }

    @Override
    public String getDescription() {
        String foodDescription = food.getDescription();
        return foodDescription + ", adding " + description;
    }

    @Override
    public double cost() {
        double foodCost = food.cost();
        return foodCost + 4.5;
    }
}
