package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, Double salary) {
        this.name = name;
        if (salary >= 70000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary terlalu rendah!");
        }
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
