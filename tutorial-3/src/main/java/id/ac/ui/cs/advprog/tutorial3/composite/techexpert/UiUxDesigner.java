package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, Double salary) {
        this.name = name;
        if (salary >= 90000.00) {
            this.salary = salary;
        } else {
            throw new IllegalArgumentException("Salary terlalu rendah!");
        }
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
