package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("Haloo I'm Model Duck!");
    }
}
