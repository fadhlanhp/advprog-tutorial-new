package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
        numReadings = 0;
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            tempSum += weatherData.getTemperature();
            if (numReadings == 0) {
                minTemp = weatherData.getTemperature();
                maxTemp = weatherData.getTemperature();
            } else {
                if (weatherData.getTemperature() >= maxTemp) {
                    maxTemp = weatherData.getTemperature();
                } else if (weatherData.getTemperature() <= minTemp) {
                    minTemp = weatherData.getTemperature();
                }
            }
            numReadings += 1;
            display();
        }
    }
}
