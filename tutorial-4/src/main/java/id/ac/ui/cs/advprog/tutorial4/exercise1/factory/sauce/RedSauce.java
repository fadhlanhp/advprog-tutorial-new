package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class RedSauce implements Sauce {
    public String toString() {
        return "Tomato Sauce with a really good canned tomatoes";
    }
}
