package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SoftShellClams implements Clams {
    public String toString() {
        return "Frozen Clams from Depok";
    }
}
