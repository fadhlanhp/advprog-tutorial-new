package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GreenPepperTest {
    private GreenPepper greenPepper;

    @Before
    public void setUp() {
        greenPepper = new GreenPepper();
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Green Pepper", greenPepper.toString());
    }
}
