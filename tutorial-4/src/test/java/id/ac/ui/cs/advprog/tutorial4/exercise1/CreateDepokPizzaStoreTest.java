package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class CreateDepokPizzaStoreTest {
    private PizzaStore depokPizzaStore;
    private Pizza cheesePizza;
    private Pizza veggiePizza;
    private Pizza clamPizza;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
        cheesePizza = depokPizzaStore.orderPizza("cheese");
        veggiePizza = depokPizzaStore.orderPizza("veggie");
        clamPizza = depokPizzaStore.orderPizza("clam");
    }

    @Test
    public void testCheesePizza() {
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "Medium Crust Dough, not thin, not thick, just a Medium.\n" +
                "Tomato Sauce with a really good canned tomatoes\n" +
                "Shredded Cheddar\n", cheesePizza.toString());
    }

    @Test
    public void testVeggiePizza() {
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "Medium Crust Dough, not thin, not thick, just a Medium.\n" +
                "Tomato Sauce with a really good canned tomatoes\n" +
                "Shredded Cheddar\n" +
                "Garlic, Mushrooms, Green Pepper, Onion, Black Olives\n", veggiePizza.toString());
    }

    @Test
    public void testClamPizza() {
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "Medium Crust Dough, not thin, not thick, just a Medium.\n" +
                "Tomato Sauce with a really good canned tomatoes\n" +
                "Shredded Cheddar\n" +
                "Frozen Clams from Depok\n", clamPizza.toString());
    }

}
