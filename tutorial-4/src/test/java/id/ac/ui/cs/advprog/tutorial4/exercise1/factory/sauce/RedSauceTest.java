package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RedSauceTest {
    private RedSauce redSauce;

    @Before
    public void setUp() {
        redSauce = new RedSauce();
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Tomato Sauce with a really good canned tomatoes", redSauce.toString());
    }


}
