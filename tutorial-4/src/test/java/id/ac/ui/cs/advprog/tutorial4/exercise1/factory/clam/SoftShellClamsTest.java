package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SoftShellClamsTest {
    private SoftShellClams softShellClams;

    @Before
    public void setUp() {
        softShellClams = new SoftShellClams();
    }

    @Test
    public void testToStringMethod() {
        assertEquals("Frozen Clams from Depok", softShellClams.toString());
    }
}
